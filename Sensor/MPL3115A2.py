# This library is retrieved from :
# https://github.com/ControlEverythingCommunity/MPL3115A2/blob/master/Python/MPL3115A2.py
# The following code is announced in free-will license
# Modifications have been made to integrate this library into app

# ============================================================================================================
# Distributed with a free-will license.
# Use it any way you want, profit or free, provided it fits in the licenses of its associated works.
# MPL3115A2
# This code is designed to work with the MPL3115A2_I2CS I2C Mini Module available from ControlEverything.com.
# https://www.controleverything.com/products
# ============================================================================================================

# Import custom modules
import SharedInstances
# Import python modules
from threading import Thread
import time
# Import third party modules
from smbus2 import SMBus


class SensorManager(Thread):
    def __init__(self):
        # Create bus
        self.bus = SMBus(1)

        # Create logger
        self.logger = SharedInstances.__LOGGER_GENERATOR__.getLogger(__name__)

        # Store temperature and pressure for retrieve
        self.currentTemperature = 0
        self.currentPressure = 0

        # The flag which will be used to control loop
        self.stop = False

        # Share self with other threads
        SharedInstances.__SENSOR_MANAGER__ = self

        Thread.__init__(self)

    def run(self):
        while not self.stop:
            # MPL3115A2 address, 0x60(96)
            # Select control register, 0x26(38)
            #		0xB9(185)	Active mode, OSR = 128, Altimeter mode
            self.bus.write_byte_data(0x60, 0x26, 0xB9)
            # MPL3115A2 address, 0x60(96)
            # Select data configuration register, 0x13(19)
            #		0x07(07)	Data ready event enabled for altitude, pressure, temperature
            self.bus.write_byte_data(0x60, 0x13, 0x07)
            # MPL3115A2 address, 0x60(96)
            # Select control register, 0x26(38)
            #		0xB9(185)	Active mode, OSR = 128, Altimeter mode
            self.bus.write_byte_data(0x60, 0x26, 0xB9)

            time.sleep(1)

            # MPL3115A2 address, 0x60(96)
            # Read data back from 0x00(00), 6 bytes
            # status, tHeight MSB1, tHeight MSB, tHeight LSB, temp MSB, temp LSB
            data = self.bus.read_i2c_block_data(0x60, 0x00, 6)

            # Convert the data to 20-bits
            temp = ((data[4] * 256) + (data[5] & 0xF0)) / 16
            self.currentTemperature = temp / 16.0

            # MPL3115A2 address, 0x60(96)
            # Select control register, 0x26(38)
            #		0x39(57)	Active mode, OSR = 128, Barometer mode
            self.bus.write_byte_data(0x60, 0x26, 0x39)

            time.sleep(1)

            # MPL3115A2 address, 0x60(96)
            # Read data back from 0x00(00), 4 bytes
            # status, pres MSB1, pres MSB, pres LSB
            data = self.bus.read_i2c_block_data(0x60, 0x00, 4)

            # Convert the data to 20-bits
            pres = ((data[1] * 65536) + (data[2] * 256) + (data[3] & 0xF0)) / 16
            self.currentPressure = (pres / 4.0) / 1000.0

        self.logger.debug("Sensor Manager thread has been terminated.")

    def getCurrentTemperature(self):
        return self.currentTemperature

    def getCurrentPressure(self):
        return self.currentPressure

    def terminate(self):
        self.stop = True
