# Import custom modules
import SharedInstances
# Import python modules
from picamera import PiCamera
from picamera.array import PiRGBArray
from threading import Thread
import time


class VideoRecorder(Thread):
    def __init__(self, rawQueue):
        # Maintain a logger
        self.logger = SharedInstances.__LOGGER_GENERATOR__.getLogger(__name__)

        # This variable stores the Pi Camera Object
        self.camera = None

        # The queue which is used to send raw frames
        self.rawQueue = rawQueue

        # Video Quality Settings
        self.videoQuality = {"low": (320, 240), "medium": (640, 480), "high": (1280, 960)}
        self.currentQuality = "medium"

        # The flag which will be used when the request of changing quality arrived
        self.requestChangeQuality = False

        # The flag which will be used to control loops
        self.stop = False

        Thread.__init__(self)

    def setupCamera(self):
        self.camera = PiCamera()
        self.camera.resolution = self.videoQuality[self.currentQuality]
        self.camera.framerate = 10

    def run(self):
        while not self.stop:
            # Initialise the camera
            self.setupCamera()
            # The array list which stores the raw frames
            rawCapture = PiRGBArray(self.camera, size=self.videoQuality[self.currentQuality])
            try:
                # Get each frame captured and put them into raw frame queue
                for rawFrame in self.camera.capture_continuous(rawCapture, format="rgb", use_video_port=True):
                    # Send both frame, timestamp and quality
                    self.rawQueue.put((rawFrame.array, int(time.time() * 1000), self.videoQuality))
                    # Clean up cache
                    rawCapture.truncate(0)
                    if self.requestChangeQuality:
                        self.camera.close()
                        break
                    # Break the inner loop based on stop flag.
                    if self.stop:
                        break
            except:
                self.logger.error("An error occured when capturing video. Restart the video recorder thread.")
        self.logger.debug("Video recorder thread terminated.")

    def changeQuality(self, newQuality):
        self.requestChangeQuality = True
        self.currentQuality = newQuality

    def terminate(self):
        self.stop = True
