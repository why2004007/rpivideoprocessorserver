# Local modules
from VideoRecorder import VideoRecorder
from MotionDetector import MotionDetector
import SharedInstances
from Utilities import Logging
# Third party modules
from threading import Thread
from Queue import Queue


class VideoProcessor(Thread):
    def __init__(self):
        # Store logger to maintain logs
        self.logger = SharedInstances.__LOGGER_GENERATOR__.getLogger(__name__)

        # The receivers list
        self.receivers = []

        # The queues which will be used to communicate between threads
        self.rawQueue = Queue()
        self.processedFrameQueue = Queue()

        # Assign self as global video processor
        SharedInstances.__VIDEO_PROCESSOR__ = self

        # Setup and run threads
        self.recorder = VideoRecorder(self.rawQueue)
        self.detector = MotionDetector(self.rawQueue, self.processedFrameQueue)
        self.recorder.start()
        self.logger.debug("Video Recorder thread started.")
        self.detector.start()
        self.logger.debug("Motion Detector thread started.")

        # Stop flag which will be used to control loop
        self.stop = False

        self.logger.debug("Video processor initialised.")

        Thread.__init__(self)

    def run(self):
        while not self.stop:
            try:
                # Receive frames
                frame, timestamp = self.processedFrameQueue.get()
                for receiver in self.receivers:
                    receiver.put(frame)
            except:
                self.logger.error("Queue get process timeout. Terminate thread.")
                break
        self.logger.debug("Video Processor thread terminated.")

    def changeQuality(self, quality):
        self.logger.debug("Received change quality request.")
        self.recorder.changeQuality(quality)

    # Allow request handlers to add their queue to receive frames
    def registNewReceiver(self, receiver):
        self.logger.debug("Received register new receiver request.")
        self.receivers.append(receiver)

    # Allow request handlers to remove their queues
    def unregistReceiver(self, receiver):
        self.logger.debug("Received remove receiver request.")
        self.receivers.remove(receiver)

    # Handle the terminate request to the thread
    def terminate(self):
        self.recorder.terminate()
        self.detector.terminate()
        self.stop = True

    # Add the monitoring area
    def addSensitiveArea(self, areaId, area):
        self.logger.debug("Received add sensitive area request.")
        return SharedInstances.__DB_HANDLER__.addSensitiveArea(areaId, area)

    # Remove areas
    def removeSensitiveArea(self, areaId):
        self.logger.debug("Received remove sensitive area request.")
        return SharedInstances.__DB_HANDLER__.removeSensitiveArea(areaId)
