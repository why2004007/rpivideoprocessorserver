# Import custom modules
import SharedInstances
from Storage.Settings import __SCREENSHOT_FOLDER__ as screenshotFolder
# Import third party modules
import cv2
# Import python modules
from threading import Thread
import time
import os


# This class will be a new thread which handles sensitive area detection
class SensitiveAreaDetector(Thread):
    def __init__(self, queue):
        # Logger
        self.logger = SharedInstances.__LOGGER_GENERATOR__.getLogger(__name__)

        # This queue will be used to maintain the adding and remove of the queue
        self.boundingOfMovingObjectList = queue

        # The flag which will be used to control loop
        self.stop = False

        # Becasue when the camera heats up, the sensitive area will be initialised
        # Therefore, the first several seconds will all be false positives.
        # So we have to store init time and check the actual time with this to store records.
        self.initTime = int(time.time() * 1000)

        Thread.__init__(self)

    def run(self):
        self.logger.debug("Sensitive area detector thread started.")
        while not self.stop:

            # Get boundings
            frame, boundings, timestamp = self.boundingOfMovingObjectList.get()

            # Because multiple areas can be entered. We have to store all entered areas in the list
            overAllAreaEntered = []

            isSensitiveAreaEntered = False

            # Detect the entering of areas
            for bounding in boundings:
                # Check whether sensitive area has been entered
                # Handle sensitive location monitoring
                areaHasEntered, areaEntered = self.isEnteringSensitiveLocation(bounding)
                # Because we are processing multiple boundings, we should not rely on the last record
                if areaHasEntered:
                    isSensitiveAreaEntered = True
                for areaId in areaEntered:
                    overAllAreaEntered.append(areaId)

            # Store current frame as picture and add record to db
            if isSensitiveAreaEntered:

                # Check whether area has been added recently
                isAreaRecentAdded = False
                for areaId in overAllAreaEntered:
                    if SharedInstances.__DB_HANDLER__.isRecordRecentAdded(areaId):
                        isAreaRecentAdded = True

                # Do not capture the sensitive area log at the first 5 seconds.
                if int(time.time() * 1000) - self.initTime < 5000:
                    isAreaRecentAdded = True

                # Add area if it is not added recently
                if not isAreaRecentAdded:
                    filename = self.storeScreenshot(frame, timestamp)
                    for areaId in overAllAreaEntered:
                        self.logger.debug("Log area {0} has been entered.".format(areaId))
                        SharedInstances.__DB_HANDLER__.addEnteringSensitiveAreaRecord(areaId, timestamp, filename)
        self.logger.debug("Sensitive area detector thread has been terminated.")

    # Store the frame into a jpg file, return the filename
    # The filename and path is generated based on settings
    def storeScreenshot(self, frame, timestamp):
        filename = "record_{0}.jpg".format(timestamp)
        # Duplicate the filename to make sure we still have original filename
        ofileName = filename
        if screenshotFolder != '':
            if os.path.exists(screenshotFolder):
                if os.path.isfile(screenshotFolder):
                    self.logger.debug("Directory '{0}' does not exist, create it.".format(screenshotFolder))
                    os.mkdir(screenshotFolder)
            else:
                self.logger.debug("Directory '{0}' does not exist, create it.".format(screenshotFolder))
                os.mkdir(screenshotFolder)
            filename = os.path.join(screenshotFolder, filename)
        bgrFrame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
        cv2.imwrite(filename, bgrFrame)
        self.logger.debug("Screenshot has been saved as '{0}'.".format(filename))
        return ofileName

    # This function detects whether point is inside monitoring area
    def isEnteringSensitiveLocation(self, movingObject):
        isSensitiveAreaEntered = False
        areaEntered = []
        for areaEntry in SharedInstances.__DB_HANDLER__.getSensitiveAreas():
            area = (areaEntry["area"]["x"], areaEntry["area"]["y"], areaEntry["area"]["w"], areaEntry["area"]["h"])
            areaId = areaEntry["areaId"]
            if float(self.calculateOverLappingArea(movingObject, area)) / self.getSizeOfRectangle(area) > 0.3:
                isSensitiveAreaEntered = True
                areaEntered.append(areaId)
        return isSensitiveAreaEntered, areaEntered

    # This function will detect whether two rectangles are overlapping
    def isOverLapping(self, rectangle1, rectangle2):
        # The passing in rectangle is a tuple which constitutes with four elements (x, y, w, h)
        # Convert x, y, width, height into points
        upperLeftCornerOfRect1 = (rectangle1[0], rectangle1[1])
        bottomRightCornerOfRect1 = (rectangle1[0] + rectangle1[2], rectangle1[1] + rectangle1[3])
        upperLeftCornerOfRect2 = (rectangle2[0], rectangle2[1])
        bottomRightCornerOfRect2 = (rectangle2[0] + rectangle2[2], rectangle2[1] + rectangle2[3])
        # Detect overlapping horizontally
        if upperLeftCornerOfRect1[0] > bottomRightCornerOfRect2[0] or upperLeftCornerOfRect2[0] > \
                bottomRightCornerOfRect1[0]:
            return False
        # Detect overlapping vertically
        if bottomRightCornerOfRect1[1] < upperLeftCornerOfRect2[1] or bottomRightCornerOfRect2[1] < \
                upperLeftCornerOfRect1[1]:
            return False
        return True

    # This function will determine giving point is inside rectangle or not
    def isPointWithinRectangle(self, point, rectangle):
        return point[0] >= rectangle[0] and point[0] <= rectangle[0] + rectangle[2] and point[1] >= rectangle[1] and \
               point[1] <= rectangle[1] + rectangle[3]

    # This method will calculate the overlapping area of two rectangle
    def calculateOverLappingArea(self, rectangle1, rectangle2):
        # Convert x, y, width, height into points
        upperLeftCornerOfRect1 = (rectangle1[0], rectangle1[1])
        bottomRightCornerOfRect1 = (rectangle1[0] + rectangle1[2], rectangle1[1] + rectangle1[3])
        upperLeftCornerOfRect2 = (rectangle2[0], rectangle2[1])
        bottomRightCornerOfRect2 = (rectangle2[0] + rectangle2[2], rectangle2[1] + rectangle2[3])
        # If rectangle 1 in rectangle 2
        if self.isPointWithinRectangle(upperLeftCornerOfRect1, rectangle2) and self.isPointWithinRectangle(
                bottomRightCornerOfRect1, rectangle2):
            return rectangle1[2] * rectangle1[3]
        # If rectangle 2 in rectangle 1
        if self.isPointWithinRectangle(upperLeftCornerOfRect2, rectangle1) and self.isPointWithinRectangle(
                bottomRightCornerOfRect2, rectangle1):
            return rectangle2[2] * rectangle2[3]
        # Handle the partial overlapping problem

        # Option 1:
        # upper left corner of rectangle 1 is inside rectangle 2
        if self.isPointWithinRectangle(upperLeftCornerOfRect1, rectangle2):
            return (bottomRightCornerOfRect2[0] - upperLeftCornerOfRect1[0]) * (
                bottomRightCornerOfRect2[1] - upperLeftCornerOfRect1[1])

        # Option 2:
        # bottom right corner of rectangle 1 is inside rectangle 2
        if self.isPointWithinRectangle(bottomRightCornerOfRect1, rectangle2):
            return (bottomRightCornerOfRect1[0] - upperLeftCornerOfRect2[0]) * (
                bottomRightCornerOfRect1[1] - upperLeftCornerOfRect2[1])

        # Option 3:
        # upper left corner of rectangle 2 is inside rectangle 1
        if self.isPointWithinRectangle(upperLeftCornerOfRect1, rectangle2):
            return (bottomRightCornerOfRect1[0] - upperLeftCornerOfRect2[0]) * (
                bottomRightCornerOfRect1[1] - upperLeftCornerOfRect2[1])

        # Option 4:
        # bottom right corner of rectangle 2 is inside rectangle 1
        if self.isPointWithinRectangle(bottomRightCornerOfRect1, rectangle2):
            return (bottomRightCornerOfRect2[0] - upperLeftCornerOfRect1[0]) * (
                bottomRightCornerOfRect2[1] - upperLeftCornerOfRect1[1])

        # No overlap at all
        return 0

    # This method will return the size of an rectangle
    def getSizeOfRectangle(self, rectangle):
        return rectangle[2] * rectangle[3]

    def terminate(self):
        self.stop = True
