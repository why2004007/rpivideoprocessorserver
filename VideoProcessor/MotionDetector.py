# Local modules
import SharedInstances
from SensitiveAreaDetector import SensitiveAreaDetector
# Third party modules
from threading import Thread
import datetime
import os.path
import cv2
import time
import numpy
from Queue import Queue


class MotionDetector(Thread):
    def __init__(self, rawQueue, processedQueue):
        # Generate logger for this module
        self.logger = SharedInstances.__LOGGER_GENERATOR__.getLogger(__name__)
        # The queue which passes in raw frames
        self.rawQueue = rawQueue
        # The queue which sends processed frames
        self.processedQueue = processedQueue
        # The queue which sends boundings and frames
        self.boundingsQueue = Queue()
        # Create sensitive area detector
        self.sensitiveAreaDetector = SensitiveAreaDetector(self.boundingsQueue)
        self.sensitiveAreaDetector.start()
        # The flag which will be used to control loop
        self.stop = False
        # The filter which automatically calculates the background
        self.backgroundSubtractor = cv2.bgsegm.createBackgroundSubtractorMOG()  # createBackgroundSubtractorMOG2(detectShadows=False)#bgsegm.createBackgroundSubtractorMOG(backgroundRatio=0.1, noiseSigma=1.0)#createBackgroundSubtractorGMG()#createBackgroundSubtractorKNN()#bgsegm.createBackgroundSubtractorMOG()#2(detectShadows=False)
        # self.backgroundSubtractor = cv2.createBackgroundSubtractorMOG2(history=50, detectShadows=False)
        # To eliminate the false positive at the first
        # several seconds, store the initialise time
        self.initTime = int(time.time() * 1000)
        Thread.__init__(self)

    def run(self):
        self.lastDetection = 0
        while not self.stop:
            try:
                # Get frame and timestamp from raw queue
                frame, timestamp, videoQuality = self.rawQueue.get()
                currentTimestamp = int(time.time() * 1000)
                # Drop any frame if the timestamp is 500 ms old
                if currentTimestamp - timestamp > 500:
                    continue

                # Detect moving objects
                _, movingObjects = self.detectMovingObjects(frame.copy(), self.backgroundSubtractor)
                self.lastDetection = currentTimestamp

                # Draw a rectangle above moving objects
                boundings = self.drawBoundingRect(frame, movingObjects)

                # Add timestamp to frame
                self.addTimestamp(frame)

                # Send boundings and frame to the sensitive area detection thread
                self.boundingsQueue.put((frame, boundings, timestamp))

                # Put processed frame into queue
                self.processedQueue.put((frame, timestamp))
            except Exception, e:
                self.logger.error(e)
                self.logger.error("Queue get process timeout. Terminate thread.")
                break
        self.logger.debug("Motion Detector thread terminated.")

    # This function detects the moving objects
    def detectMovingObjects(self, frame, filter):
        # self.logger.debug("Start to detect moving objects.")
        # Based on the filter, subtract background
        foregroundMask = filter.apply(frame)
        # Combine small pixels
        # foregroundMask = cv2.dilate(foregroundMask, None, iterations=5)

        kernel = numpy.ones((20, 20), numpy.uint8)
        foregroundMask = cv2.dilate(foregroundMask, None, iterations=1)

        # Based on white pixels, generate joined areas
        (_, contours, _) = cv2.findContours(foregroundMask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        return foregroundMask, contours

    # This function will draw bounding rect angle above moving object
    # and detect the entering of sensitive area
    def drawBoundingRect(self, frame, contours):
        # self.logger.debug("Start to draw rectangle above moving object.")

        # This list will store the boundaries of moving objects.
        # The it will be passed to sensitive area detection thread to detech wether it
        # enters the area.
        boundingOfMovingObjectList = []

        # Draw rectangles on frame
        for contour in contours:
            # If the contour area is too small, consider it as a false positive
            if cv2.contourArea(contour) < 200:
                continue
            # Calculate the bounding rectangle of this contour
            (x, y, w, h) = cv2.boundingRect(contour)
            # Draw rectangle
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
            # This bounding will be send to sensitive checking thread to check the overlapping
            boundingOfMovingObject = (x, y, w, h)
            boundingOfMovingObjectList.append(boundingOfMovingObject)
        # self.logger.debug("Detect {0} large moving object. {1} areas have been entered.".format(largeMovingObject, len(
        #    overAllAreaEntered)))
        return boundingOfMovingObjectList

    # This method will add timestamp to frame
    def addTimestamp(self, frame):
        # Get current time and convert it into text
        timestamp = datetime.datetime.now()
        timeText = timestamp.strftime('%d %B %Y %I:%M:%S.%f')[:-3]
        # Calculate the text size
        size = cv2.getTextSize(timeText, cv2.FONT_HERSHEY_SIMPLEX, 0.5, 2)[0]
        # Draw background under text
        cv2.rectangle(frame, (10, 30), (10 + size[0], 30 - size[1]), (0, 0, 0), -1)
        # Put text above the background
        cv2.putText(frame, timeText, (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), thickness=2)


    def terminate(self):
        self.sensitiveAreaDetector.terminate()
        self.stop = True
