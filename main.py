# Import custom module
from RequestHandlers.VideoStreamHandler import VideoStreamHandler
from RequestHandlers.LoginHandler import LoginHandler
from RequestHandlers.RecordsHandler import RecordsHandler
from RequestHandlers.SensitiveAreaHandler import SensitiveAreaHandler
from RequestHandlers.ChangeVideoQualityHandler import ChangeVideoQualityHandler
from RequestHandlers.PhotoHandler import PhotoHandler
from RequestHandlers.SensorReadingHandler import SensorReadingHandler
from RequestHandlers.StorageHandler import StorageHandler
from VideoProcessor.VideoProcessor import VideoProcessor
from Storage.DBHandler import DBHandler
from Sensor.MPL3115A2 import SensorManager
from Utilities import Logging
import SharedInstances
# Import third party module
import tornado.ioloop
import tornado.web
import tornado.options

def make_app():
    return tornado.web.Application([
        (r'/login', LoginHandler),
        (r'/change-quality', ChangeVideoQualityHandler),
        (r'/records', RecordsHandler),
        (r'/areas', SensitiveAreaHandler),
        (r'/video', VideoStreamHandler),
        (r'/photo', PhotoHandler),
        (r'/sensor', SensorReadingHandler),
        (r'/storage', StorageHandler)
    ])

def main():
    # Initialise logging environment
    Logging.Logging()

    # Initialise DB Handler before video processor
    # Because db handler may be called by video processor
    dbHandler = DBHandler()
    dbHandler.start()

    # Initialise video processor
    videoProcessor = VideoProcessor()
    videoProcessor.start()

    # Initialise sensor manager
    sensorManager = SensorManager()
    sensorManager.start()

    try:
        app = make_app()
        app.listen(8000)
        tornado.ioloop.IOLoop.current().start()
    except Exception as e:
        SharedInstances.__LOGGER_GENERATOR__.getLogger(__name__).critical(
            'Exception occured, message: {0}'.format(e.message))
    finally:
        # During the terminate phase of app
        # Terminate web server first, to disconnect all clients
        # Then terminate video processor so that db handler still
        #  can store unfinished process
        tornado.ioloop.IOLoop.current().stop()
        videoProcessor.terminate()
        dbHandler.terminate()
        sensorManager.terminate()

if __name__ == '__main__':
    main()
