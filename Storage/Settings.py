# The following part will be used by DBHandler to add default user
__DEFAULT_USER_USERNAME__ = 'admin'
__DEFAULT_USER_PASSWORD__ = 'nimda'
__DATABASE_FILENAME__ = 'main.db'

# The following part will be used by AuthroizationCheck module
# Only permit json as body
__PERMITED_CONTENT_TYPE__ = 'application/json'
# Only permit requests which comes from ios app
__PERMITED_USER_AGENT_PREFIX__ = 'CatCaring/'

# This error code list will be used to generate responses
__FAILED_DECODE_REQUEST__ = -1
__FAILED_DECODE_REQUEST_MESSAGE__ = 'Failed to decode request body.'
__INCORRECT_REQUEST_BODY__ = -2
__INCORRECT_REQUEST_BODY_MESSAGE__ = 'Incorrect format of request body'
__AUTHENTICATION_CHECK_FAILED__ = -3
__AUTHENTICATION_CHECK_FAILED_TEXT__ = 'Authentication failed.'
__FAILED_TO_REMOVE_SENSITIVE_AREA__ = -4
__DUPLICATE_AREA_ID_ERROR__ = -5
__RECORD_DOES_NOT_EXIST__ = -6

# This succeed code list will be used to generate response
__OK__ = 0

# The following part will be used by loggers
__LOGGER_FORMAT__ = '%(asctime)s: [%(name)s] [%(levelname)s]: %(message)s'

# The file log level should always be lower than console log level, otherwise
# high level log will be discarded.
# The log level must be one of these: DEBUG, INFO, WARN, ERROR, CRITICAL
__FILE_LOG_LEVEL__ = 'DEBUG'
__CONSOLE_LOG_LEVEL__ = 'DEBUG'
__LOGGING_FOLDER__ = 'log'

# The following setting will be used to specify where screenshot will be stored
__SCREENSHOT_FOLDER__ = 'screenshot'

# The following setting will be used to specify cookie behaviour
# By default, cookie will expire in one hour
__COOKIE_EXPIRATION__ = 3600 * 1000
