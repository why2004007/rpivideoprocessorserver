# Local modules
from Settings import \
    __DEFAULT_USER_USERNAME__ as defaultUsername, \
    __DEFAULT_USER_PASSWORD__ as defaultPassword, \
    __DATABASE_FILENAME__ as dbName, \
    __COOKIE_EXPIRATION__ as cookieExpiration, \
    __SCREENSHOT_FOLDER__ as screenshotFolder
import SharedInstances
from Utilities import Logging
# Third party modules
from threading import Thread
import sqlite3
import hashlib
import time
import os

# Constant operation code variables
OPERATION_ADD_USER = 0
OPERATION_REMOVE_USER = 1
OPERATION_ADD_SENSITIVE_AREA = 2
OPERATION_REMOVE_SENSITIVE_AREA = 3
OPERATION_ADD_ENTERING_SENSITIVE_AREA_RECORD = 4
OPERATION_REMOVE_ENTERING_SENSITIVE_AREA_RECORD = 5


# This class handles the data storage and check
class DBHandler(Thread):
    def __init__(self):
        # The logger which will be used to log files
        self.logger = SharedInstances.__LOGGER_GENERATOR__.getLogger(__name__)

        # Create instance for variable storing
        self.connection = None
        self.cursor = None

        # Store users both in database and memory for thread safe reason
        self.userList = []

        # Store cookies within memory because each time server restarts, the cookie should be removed
        self.cookieList = []

        # Store sensitive areas both in db and mem
        self.sensitiveAreasList = []

        # Store records both in db and mem
        self.enteringSensitiveAreaRecordsList = []

        # To avoid sensitive area being added repeatedly,
        # this list is designed to maintain an interval between storings
        self.enteringSensitiveAreaRecordsHistory = []

        # Store the operations within a list to ensure thread safe
        self.operationList = []

        # Stop flag which will be used to control loop
        self.stop = False

        # After initialization, assign self as global db handler
        SharedInstances.__DB_HANDLER__ = self

        Thread.__init__(self)

    # To avoid thread safe warning, move all init process into run loop
    def initDB(self):
        # Establish connection to database
        self.initDBConnection(dbName)

        # Check whether default exists or not, if not add it
        self.checkDefaultUser()

        # Load results from database
        self._loadUserList()
        self._loadSensitiveAreaList()
        self._loadEnteringSensitiveAreaRecordsList()

    # The main loop, all the operations will be finish in this loop
    def run(self):
        self.initDB()
        while not self.stop:
            # For each the whole operation list
            for operation in self.operationList:
                # Add user
                if operation["operation"] == OPERATION_ADD_USER:
                    self._addUser(operation["params"])
                    self.operationList.remove(operation)
                # Remove user
                if operation["operation"] == OPERATION_REMOVE_USER:
                    self._removeUser(operation["params"])
                    self.operationList.remove(operation)
                # Add sensitive area
                if operation["operation"] == OPERATION_ADD_SENSITIVE_AREA:
                    self._addSensitiveArea(operation["params"])
                    self.operationList.remove(operation)
                # Remove sensitive area
                if operation["operation"] == OPERATION_REMOVE_SENSITIVE_AREA:
                    self._removeSensitiveArea(operation["params"])
                    self.operationList.remove(operation)
                # add record
                if operation["operation"] == OPERATION_ADD_ENTERING_SENSITIVE_AREA_RECORD:
                    self._addEnteringSensitiveAreaRecord(operation["params"])
                    self.operationList.remove(operation)
                # remove record
                if operation["operation"] == OPERATION_REMOVE_ENTERING_SENSITIVE_AREA_RECORD:
                    self._removeEnteringSensitiveAreaRecord(operation["params"])
                    self.operationList.remove(operation)
                # Commit changes made to db.
                self.connection.commit()

            # Reduce cpu usage, sleep 500 ms after finished all operations
            time.sleep(0.5)

        # Commit and close connection before quit
        self.connection.commit()
        self.connection.close()

    def initDBConnection(self, dbName):
        # Establish connections
        self.connection = sqlite3.connect(dbName)
        self.cursor = self.connection.cursor()
        self.logger.debug("Database file {0} opened and connected.".format(dbName))

        # Create tables respectively
        self.logger.debug("Check and create tables in database.")
        self.cursor.execute(
            "CREATE TABLE IF NOT EXISTS ENTERING_AREA_RECORDS ( AREA_ID INTEGER, ENTERING_TIME INTEGER, PHOTOID INTEGER)")
        self.cursor.execute(
            "CREATE TABLE IF NOT EXISTS SENSITIVE_AREAS (AREA_ID INTEGER, X INTEGER, Y INTEGER, WIDTH INTEGER, HEIGHT INTEGER)")
        self.cursor.execute("CREATE TABLE IF NOT EXISTS USERS (USERNAME VARCHAR, PASSWORD VARCHAR )")

        self.logger.info("DB connection established.")

    # Check and add default user if default user does not exist
    def checkDefaultUser(self):
        self.logger.debug("Check if default user exists in db or not.")
        defaultUserExists = False
        for row in self.cursor.execute("SELECT * FROM USERS WHERE USERNAME = ?", (defaultUsername,)):
            if row[0] == defaultUsername:
                defaultUserExists = True
                self.logger.debug("Default user exists.")

        if not defaultUserExists:
            self._addUser({"username": defaultUsername, "password": defaultPassword})
            self.logger.debug("Default user does not exist. Create it.")
        self.logger.info("Default user check finished.")

    # The following functions must be called within db handler's main thread,
    # to avoid thread safe issue
    def _addUser(self, params):
        self.logger.debug("Start add user operation.")
        username = params["username"]
        password = params["password"]
        passwordHash = hashlib.md5(password).hexdigest()
        self.logger.debug("Add user: {0}.".format(username))
        self.cursor.execute("INSERT INTO USERS VALUES(?, ?)", (username, passwordHash))

    def _removeUser(self, params):
        self.logger.debug("Start remove user operation.")
        username = params["username"]
        password = params["password"]
        passwordHash = hashlib.md5(password).hexdigest()
        self.logger.debug("Remove user: {0}.".format(username))
        self.cursor.execute("DELETE FROM USERS WHERE USERNAME = ? AND PASSWORD = ?", (username, passwordHash))

    def _loadUserList(self):
        self.logger.debug("Start load user list operation.")
        self.userList = []
        for row in self.cursor.execute("SELECT * FROM USERS"):
            self.userList.append({"username": row[0], "password": row[1]})
        self.logger.info("{0} users have been loaded.".format(len(self.userList)))

    def _addSensitiveArea(self, params):
        self.logger.debug("Start add sensitive area operation.")
        areaId = params["areaId"]
        area = params["area"]
        self.logger.debug(
            "Add sensitive area: id: {0}, x: {1}, y: {2}, w: {3}, h: {4}".format(areaId, area["x"], area["y"],
                                                                                 area["w"], area["h"]))
        self.cursor.execute("INSERT INTO SENSITIVE_AREAS VALUES (?, ?, ?, ?, ?)", (areaId, area["x"], area["y"],
                                                                                   area["w"], area["h"]))

    def _removeSensitiveArea(self, params):
        self.logger.debug("Start remove sensitive area operation.")
        areaId = params["areaId"]
        self.logger.debug("Removing areaid: {0} from list.".format(areaId))
        self.cursor.execute("DELETE FROM SENSITIVE_AREAS WHERE AREA_ID = ?", (areaId,))

    def _loadSensitiveAreaList(self):
        self.logger.debug("Start load sensitive area list operation.")
        self.sensitiveAreasList = []
        for row in self.cursor.execute("SELECT * FROM SENSITIVE_AREAS"):
            self.sensitiveAreasList.append(
                {"areaId": row[0], "area": {"x": row[1], "y": row[2], "w": row[3], "h": row[4]}})
        self.logger.info("{0} sensitive areas have been loaded.".format(len(self.sensitiveAreasList)))

    def _addEnteringSensitiveAreaRecord(self, params):
        self.logger.debug("Start add entering sensitive area record operation.")
        areaId = params["areaId"]
        enteringTime = params["timestamp"]
        filename = params["photoId"]
        self.logger.debug(
            "Add record: areaid: {0}, timestamp: {1}, filename: {2}.".format(areaId, enteringTime, filename))
        self.cursor.execute("INSERT INTO ENTERING_AREA_RECORDS VALUES (?, ?, ?)", (areaId, enteringTime, filename))

    def _removeEnteringSensitiveAreaRecord(self, params):
        self.logger.debug("Start remove entering sensitive area record operation.")
        areaId = params["areaId"]
        enteringTime = params["timestamp"]
        self.cursor.execute("DELETE FROM ENTERING_AREA_RECORDS WHERE AREA_ID = ? AND ENTERING_TIME = ?",
                            (areaId, enteringTime))

    def _loadEnteringSensitiveAreaRecordsList(self):
        self.logger.debug("Start load entering sensitive area records list operation.")
        self.enteringSensitiveAreaRecordsList = []
        for row in self.cursor.execute("SELECT * FROM ENTERING_AREA_RECORDS"):
            self.enteringSensitiveAreaRecordsList.append({"areaId": row[0], "timestamp": row[1], "photoId": row[2]})
        self.logger.info(
            "{0} sensitive area records have been loaded.".format(len(self.enteringSensitiveAreaRecordsList)))

    # Thread safe functions, these functions should be called as normal functions
    def addUser(self, username, password):
        self.logger.debug("Receive request to add user with username: {0}.".format(username))
        passwordHash = hashlib.md5(password).hexdigest()
        self.userList.append({"username": username, "password": passwordHash})
        # Remember to pass in original password instead of has because later when inserting into db, the password will be hashed again
        self.addOperation(OPERATION_ADD_USER, {"username": username, "password": password})

    # This function handles remove user, it will try to remove user
    # Return True on succeed
    def removeUser(self, username, password):
        self.logger.debug(
            "Receive request to remove user with username: " + username + " and password: " + password + ".")
        for user in self.userList:
            if user["username"] == username:
                passwordHash = hashlib.md5(password).hexdigest()
                if user["password"] == passwordHash:
                    self.userList.remove(user)
                    self.addOperation(OPERATION_REMOVE_USER, {"username": username, "password": password})
                    self.logger.debug(
                        "Find user with given username: {0}. This user has been removed.".format(username))
                    return True
        self.logger.debug(
            "Did not find the user with given username {0} and password. Operation abort.".format(username))
        return False

    # This function add add sensitive area operation to operation list
    def addSensitiveArea(self, areaId, area):
        isAreaIdExists = False
        for areaEntry in self.sensitiveAreasList:
            if areaEntry['areaId'] == areaId:
                isAreaIdExists = True
        if not isAreaIdExists:
            self.logger.debug(
                "Receive request to add sensitive area with areaId: {0}, x: {1}, y: {2}, w: {3}, h: {4}.".format(areaId,
                                                                                                                 area[
                                                                                                                     0],
                                                                                                                 area[
                                                                                                                     1],
                                                                                                                 area[
                                                                                                                     2],
                                                                                                                 area[
                                                                                                                     3]))
            areaEntry = {"areaId": areaId, "area": {"x": area[0], "y": area[1], "w": area[2], "h": area[3]}}
            self.sensitiveAreasList.append(areaEntry)
            self.addOperation(OPERATION_ADD_SENSITIVE_AREA, areaEntry)
            return True
        else:
            return False

    # This function will remove sensitive with given id
    # Return True on succeed.
    def removeSensitiveArea(self, areaId):
        self.logger.debug("Receive request to remove sensitive area with areaid: {0}.".format(areaId))
        for areaEntry in self.sensitiveAreasList:
            if areaEntry["areaId"] == areaId:
                self.sensitiveAreasList.remove(areaEntry)
                self.addOperation(OPERATION_REMOVE_SENSITIVE_AREA, {"areaId": areaId})
                self.logger.debug("Area with areaid: {0} has been found. Remove succeed.".format(areaId))
                return True
        self.logger.debug("Did not find area with areaid: {0}. Operation abort.".format(areaId))
        return False

    # This function will remove all sensitive area
    def removeAllSensitiveArea(self):
        self.logger.debug("Recived request to remove all sensitive area.")
        for areaEntry in self.sensitiveAreasList:
            self.sensitiveAreasList.remove(areaEntry)
            self.addOperation(OPERATION_REMOVE_SENSITIVE_AREA, {"areaId": areaEntry["areaId"]})

    # This function will return all sensitive area
    def getSensitiveAreas(self):
        return self.sensitiveAreasList

    # This function adds entering sensitive area records to operation list
    def addEnteringSensitiveAreaRecord(self, areaId, enteringTime, filename):
        self.logger.debug(
            "Receive add entering sensitive record request. areaId: {0}, timestamp: {1}, filename: {2}".format(areaId,
                                                                                                               enteringTime,
                                                                                                               filename))
        recordEntry = {"areaId": areaId, "timestamp": enteringTime, "photoId": filename}
        self.enteringSensitiveAreaRecordsList.append(recordEntry)
        self.addOperation(OPERATION_ADD_ENTERING_SENSITIVE_AREA_RECORD, recordEntry)
        for recordEntry in self.enteringSensitiveAreaRecordsHistory:
            if recordEntry["areaId"] == areaId:
                self.enteringSensitiveAreaRecordsHistory.remove(recordEntry)
        self.enteringSensitiveAreaRecordsHistory.append({"areaId": areaId, "timestamp": enteringTime})

    # This function will return all records on entering sensitive area
    def getEnteringSensitiveAreaRecords(self):
        return self.enteringSensitiveAreaRecordsList

    # This function handles the remove of records
    def removeEnteringSensitiveAreaRecords(self, areaId, timestamp):
        isRecordExists = False
        photoId = None
        for recordEntry in self.enteringSensitiveAreaRecordsList:
            if recordEntry["timestamp"] == timestamp and recordEntry["areaId"] == areaId:
                photoId = recordEntry["photoId"]
                self.enteringSensitiveAreaRecordsList.remove(recordEntry)
                isRecordExists = True
        if isRecordExists:
            self.addOperation(OPERATION_REMOVE_ENTERING_SENSITIVE_AREA_RECORD,
                              {"areaId": areaId, "timestamp": timestamp})
            os.remove(os.path.join(screenshotFolder, photoId))
            return True
        return False

    # This function will remove all sensitive area records
    def removeAllEnteringSensitiveAreaRecords(self):
        self.logger.debug("Received request to remove all records.")
        for recordEntry in self.enteringSensitiveAreaRecordsList:
            self.enteringSensitiveAreaRecordsList.remove(recordEntry)
            self.addOperation(OPERATION_ADD_ENTERING_SENSITIVE_AREA_RECORD,
                              {"areaId": recordEntry["areaId"], "timestamp": recordEntry["timestamp"]})
            os.remove(os.path.join(screenshotFolder, recordEntry["photoId"]))

    # This function will add operation into operation list so that main loop can process
    def addOperation(self, operationId, params):
        # This function standardise the add operation process
        # to minimize the possibility of entering wrong command
        self.operationList.append({"operation": operationId, "params": params})

    # This function will check whether giving username and password combination exists
    def checkUserExistence(self, username, password):
        self.logger.debug("Received check user existence request, username: {0}".format(username))
        passwordHash = hashlib.md5(password).hexdigest()
        for user in self.userList:
            if user["username"] == username and user["password"] == passwordHash:
                return True
        return False

    # This function will add cookie to cookie list for future authorization checks
    def addCookie(self, cookie):
        expire = int(time.time() * 1000) + cookieExpiration
        self.cookieList.append({"cookie": cookie, "expire": expire})

    # This function will check cookie's availability
    def checkCookie(self, cookie):
        self.logger.debug("Received check cookie request {0}".format(cookie))
        # First remove all expired cookies
        for cookieEntry in self.cookieList:
            if cookieEntry["expire"] < int(time.time() * 1000):
                self.cookieList.remove(cookieEntry)
        # Then check cookie availability
        for cookieEntry in self.cookieList:
            if cookieEntry["cookie"] == cookie:
                return True
        return False

    # This function is designed to avoid record creep.
    def isRecordRecentAdded(self, areaId):
        for recordHistory in self.enteringSensitiveAreaRecordsHistory:
            if recordHistory["areaId"] == areaId:
                if int(time.time() * 1000) - recordHistory["timestamp"] < 30000:
                    return True
        return False

    # Receive the signal to kill main loop
    def terminate(self):
        self.stop = True
