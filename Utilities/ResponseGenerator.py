from Storage import Settings


# The standard response template
def generateResponse(statusCode, message, results):
    return {"status": statusCode, "message": message, 'results': results}


# Failed to decode request body response
def generateFailedToDecodeRequestBodyResponse():
    return generateResponse(Settings.__FAILED_DECODE_REQUEST__, Settings.__FAILED_DECODE_REQUEST_MESSAGE__, [])


# Incorrect request body response
def generateIncorrectRequestBodyResponse():
    return generateResponse(Settings.__INCORRECT_REQUEST_BODY__, Settings.__INCORRECT_REQUEST_BODY_MESSAGE__, [])


# Cookie check failed
def generateAuthenticationCheckFailedResponse():
    return generateResponse(Settings.__AUTHENTICATION_CHECK_FAILED__, Settings.__AUTHENTICATION_CHECK_FAILED_TEXT__, [])
