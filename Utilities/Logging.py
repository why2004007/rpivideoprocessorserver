# Import custom modules
from Storage.Settings import __LOGGER_FORMAT__ as loggerFormat, \
    __FILE_LOG_LEVEL__ as globalFileLogLevel, \
    __CONSOLE_LOG_LEVEL__ as globalConsoleLogLevel, \
    __LOGGING_FOLDER__ as loggingFolder
import SharedInstances
# Import python modules
import logging
import os.path
import time


class Logging():
    def __init__(self):
        # Convert log level from text to level
        fileLogLevel = self.getLogLevelFromText(globalFileLogLevel)
        consoleLogLevel = self.getLogLevelFromText(globalConsoleLogLevel)
        # Store file log level as a global variable within module so that other function could utilise this
        self.__FILE_LOG_LEVEL__ = fileLogLevel

        # Based on settings file, generate filename
        currentTimestamp = int(time.time() * 1000)
        filename = str(currentTimestamp) + '.log'
        if not loggingFolder == '':
            # Create logging folder if not exists
            if not os.path.exists(loggingFolder):
                os.mkdir(loggingFolder)
            filename = os.path.join(loggingFolder, filename)

        # Set global logging config
        logging.basicConfig(filename=filename, format=loggerFormat, level=fileLogLevel)

        # Initialise console handler
        self.__CONSOLE_HANDLER__ = logging.StreamHandler()
        self.__CONSOLE_HANDLER__.setLevel(consoleLogLevel)
        formatter = logging.Formatter(loggerFormat)
        self.__CONSOLE_HANDLER__.setFormatter(formatter)

        tempLogger = self.getLogger(__name__)
        tempLogger.debug('Basic config has been set.')
        tempLogger.debug('Console level logger has been initialised.')
        tempLogger.info('Logging environment initialisation finished.')
        SharedInstances.__LOGGER_GENERATOR__ = self

    def getLogger(self, name):
        # Based on module level global variables, setup logger
        logger = logging.getLogger(name)
        logger.setLevel(self.__FILE_LOG_LEVEL__)
        # Add console handler so that the output can be shown on screen
        logger.addHandler(self.__CONSOLE_HANDLER__)
        return logger

    def getLogLevelFromText(self, levelText):
        logLevel = logging.DEBUG
        if levelText == 'DEBUG':
            logLevel = logging.DEBUG
        if levelText == 'INFO':
            logLevel = logging.INFO
        if levelText == 'WARN':
            logLevel = logging.WARN
        if levelText == 'ERROR':
            logLevel = logging.ERROR
        if levelText == 'CRITICAL':
            logLevel = logging.CRITICAL
        return logLevel
