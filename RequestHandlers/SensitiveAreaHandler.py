# Import custom modules
from BaseHandler import BaseHandler
import SharedInstances
from Utilities import ResponseGenerator
from Storage import Settings
# Import third party module
import tornado.escape


# This handler will handle the request sends to path '/areas'
# This handler can add, remove, get sensitive areas.
class SensitiveAreaHandler(BaseHandler):
    def processPostRequest(self, body):
        # Check json structure
        if 'operation' and 'params' in body:
            if body['operation'] == 'add':
                if not ('areaId' and 'area' in body['params'] and 'x' and 'y' and 'w' and 'h' in body['params'][
                    'area']):
                    self.sendIncorrectRequestBodyResponse()
                    return
                areaId = body['params']['areaId']
                x = body['params']['area']['x']
                y = body['params']['area']['y']
                w = body['params']['area']['w']
                h = body['params']['area']['h']
                if SharedInstances.__DB_HANDLER__.addSensitiveArea(areaId, (x, y, w, h)):
                    response = ResponseGenerator.generateResponse(Settings.__OK__, 'Sensitive area added.', [])
                else:
                    response = ResponseGenerator.generateResponse(Settings.__DUPLICATE_AREA_ID_ERROR__,
                                                                  'Area Id {0} has already existed.'.format(areaId), [])
                self.write(tornado.escape.json_encode(response))
            if body['operation'] == 'remove':
                if not 'areaId' in body['params']:
                    self.sendIncorrectRequestBodyResponse()
                    return
                areaId = body['params']['areaId']
                if SharedInstances.__DB_HANDLER__.removeSensitiveArea(areaId):
                    response = ResponseGenerator.generateResponse(Settings.__OK__, 'Remove sensitive area succeed.', [])
                else:
                    response = ResponseGenerator.generateResponse(Settings.__FAILED_DECODE_REQUEST_MESSAGE__,
                                                                  'Failed to remove sensitive area. Area not found.',
                                                                  [])
                self.write(tornado.escape.json_encode(response))
            if body['operation'] == 'removeAll':
                SharedInstances.__DB_HANDLER__.removeAllSensitiveArea()
                response = ResponseGenerator.generateResponse(Settings.__OK__, 'All areas have been removed.', [])
                self.write(tornado.escape.json_encode(response))
            if body['operation'] == 'get':
                areas = SharedInstances.__DB_HANDLER__.getSensitiveAreas()
                response = ResponseGenerator.generateResponse(Settings.__OK__, 'Records retrieved.', areas)
                self.write(tornado.escape.json_encode(response))
        else:
            self.sendIncorrectRequestBodyResponse()
