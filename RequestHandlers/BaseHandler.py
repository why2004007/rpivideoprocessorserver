# Local modules
from Storage.Settings import __PERMITED_CONTENT_TYPE__ as permitedContentType, \
    __PERMITED_USER_AGENT_PREFIX__ as permitedUserAgentPrefix
from Utilities import ResponseGenerator
import SharedInstances
# Third party modules
import tornado.web
import tornado.escape


# This handler is the parent handler of all other handlers.
# It defines basic responses and handles the permission checks.
# Override processPostRequest to perform other operations.
class BaseHandler(tornado.web.RequestHandler):
    def __init__(self, application, request, **kwargs):
        self.logger = SharedInstances.__LOGGER_GENERATOR__.getLogger(__name__)
        tornado.web.RequestHandler.__init__(self, application, request, **kwargs)


    def get(self):
        # Reject get
        self.send_error(403)

    # Use customised wrapper to perform global authentication check
    def post(self):
        # Permission check on request
        if self.requestIsPermitted():
            try:
                if not self.authenticationCheck():
                    return
                # The passing in json body
                body = tornado.escape.json_decode(self.request.body)
                # Pass the request body to the actual handling function
                self.processPostRequest(body)
            except Exception as e:
                # Failed to decode json will trigger this exception
                self.logger.error(e)
                self.sendFailedToDecodeRequestBodyResponse()
        else:
            # HTTP 403 means Forbidden
            self.send_error(403)

    def authenticationCheck(self):
        # Get cookie for permission checks
        cookie = self.get_cookie('auth')
        # Check whether cookie is valid
        if not SharedInstances.__DB_HANDLER__.checkCookie(cookie):
            response = ResponseGenerator.generateAuthenticationCheckFailedResponse()
            self.write(tornado.escape.json_encode(response))
            return False
        return True

    def requestIsPermitted(self):
        # Get interested headers
        contentType = self.request.headers.get("Content-Type", None)
        userAgent = self.request.headers.get("User-Agent", None)
        cookie = self.request.headers.get("Cookie", None)
        # Return check results
        return (contentType == permitedContentType) and \
               (userAgent.startswith(permitedUserAgentPrefix)) and \
               (cookie is not None)

    # Send errors
    def sendFailedToDecodeRequestBodyResponse(self):
        response = ResponseGenerator.generateFailedToDecodeRequestBodyResponse()
        self.write(tornado.escape.json_encode(response))

    def sendIncorrectRequestBodyResponse(self):
        response = ResponseGenerator.generateIncorrectRequestBodyResponse()
        self.write(tornado.escape.json_encode(response))

    def processPostRequest(self, body):
        pass
