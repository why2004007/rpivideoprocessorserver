# Import custom modules
from BaseHandler import BaseHandler
import SharedInstances
from Utilities import ResponseGenerator
from Storage import Settings
# Import third party module
import tornado.escape


# This handler handles requests passed to '/records'
# This handler can get and remove records
class RecordsHandler(BaseHandler):
    def processPostRequest(self, body):
        # Check json structure
        if 'operation' in body:
            # Dispatch task based on operation
            if body["operation"] == 'get':
                records = SharedInstances.__DB_HANDLER__.getEnteringSensitiveAreaRecords()
                response = ResponseGenerator.generateResponse(Settings.__OK__, 'Records retrieved.', records)
                self.write(tornado.escape.json_encode(response))

            if body["operation"] == 'remove' and 'areaId' and 'timestamp' in body["params"]:
                areaId = body["params"]["areaId"]
                timestamp = body["params"]["timestamp"]
                if SharedInstances.__DB_HANDLER__.removeEnteringSensitiveAreaRecords(areaId, timestamp):
                    response = ResponseGenerator.generateResponse(Settings.__OK__, 'Records removed.', [])
                    self.write(tornado.escape.json_encode(response))
                else:
                    response = ResponseGenerator.generateResponse(Settings.__RECORD_DOES_NOT_EXIST__,
                                                                  'Records does not exist.', [])
                    self.write(tornado.escape.json_encode(response))

            if body['operation'] == 'removeAll':
                SharedInstances.__DB_HANDLER__.removeAllEnteringSensitiveAreaRecords()
                response = ResponseGenerator.generateResponse(Settings.__OK__, 'All records have been removed.', [])
                self.write(tornado.escape.json_encode(response))

        else:
            self.sendIncorrectRequestBodyResponse()
