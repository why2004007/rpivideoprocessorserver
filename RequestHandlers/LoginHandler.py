# Import custom modules
from Storage.Settings import __PERMITED_CONTENT_TYPE__ as permitedContentType, \
    __PERMITED_USER_AGENT_PREFIX__ as permitedUserAgentPrefix
from BaseHandler import BaseHandler
from Utilities import ResponseGenerator, Logging
import SharedInstances
from Storage import Settings
# Import python modules
import hashlib
import time
# Import third party module
import tornado.escape


# This handler handles requests passed to '/login'
class LoginHandler(BaseHandler):
    def processPostRequest(self, body):
        # Check json structure
        if 'username' and 'password' in body:
            # Check username and password combination
            if SharedInstances.__DB_HANDLER__.checkUserExistence(body["username"], body["password"]):
                # Generate cookie
                cookieBase = '{0}.{1}.{2}'.format(body["username"], body["password"], int(time.time() * 1000))
                cookie = hashlib.md5(cookieBase).hexdigest()
                # Store cookie in database handler
                SharedInstances.__DB_HANDLER__.addCookie(cookie)
                # Set cookie in response header
                self.set_cookie('auth', cookie)
                # Generate response
                response = ResponseGenerator.generateResponse(Settings.__OK__, 'Cookie generated.',
                                                              [{"cookie": cookie}])
                self.write(tornado.escape.json_encode(response))
            else:
                # Send authentication failed response
                response = ResponseGenerator.generateResponse(Settings.__AUTHENTICATION_CHECK_FAILED__,
                                                              Settings.__AUTHENTICATION_CHECK_FAILED_TEXT__, [])
                self.write(tornado.escape.json_encode(response))
        else:
            self.sendIncorrectRequestBodyResponse()

    # Overwrite authentication check to allow no cookie access
    def authenticationCheck(self):
        return True

    def requestIsPermitted(self):
        # Get interested headers
        contentType = self.request.headers.get("Content-Type", None)
        userAgent = self.request.headers.get("User-Agent", None)
        # Return check results
        return (contentType == permitedContentType) and \
               (userAgent.startswith(permitedUserAgentPrefix))
