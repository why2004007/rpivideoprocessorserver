# Import custom modules
from BaseHandler import BaseHandler
from Utilities import ResponseGenerator
from Storage import Settings
# Import python modules
import os
# Import third party module
import tornado.escape


# This handler will handle the requests send to '/storage'
# This handler will return current available space on disk and the maximum capacity of the disk
class StorageHandler(BaseHandler):
    def processPostRequest(self, body):
        if 'operation' in body:
            if body['operation'] == 'get':
                status = os.statvfs('/')
                systemAvailableStorage = (status.f_bavail * status.f_frsize) / 1024
                systemMaximumStorage = (status.f_blocks * status.f_bsize) / 1024
                availableStorageResponse = {'available': systemAvailableStorage, 'maximum': systemMaximumStorage}
                response = ResponseGenerator.generateResponse(Settings.__OK__, 'Capacity loaded.',
                                                              [availableStorageResponse])
                self.write(tornado.escape.json_encode(response))
            else:
                self.sendIncorrectRequestBodyResponse()
        else:
            self.sendIncorrectRequestBodyResponse()
