# Import custom modules
from BaseHandler import BaseHandler
import SharedInstances
from Utilities import ResponseGenerator
from Storage import Settings
# Import third party module
import tornado.escape


# This handler handles requests passed to '/change-quality'
class ChangeVideoQualityHandler(BaseHandler):
    def processPostRequest(self, body):
        if 'quality' in body:
            # Based on the quality, call video processor to change quality
            SharedInstances.__VIDEO_PROCESSOR__.changeQuality(body["quality"])
            response = ResponseGenerator.generateResponse(Settings.__OK__, "Quality change succeed.", [])
            self.write(tornado.escape.json_encode(response))
        else:
            self.sendIncorrectRequestBodyResponse()
