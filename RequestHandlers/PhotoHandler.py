# Import custom modules
from BaseHandler import BaseHandler
from Storage.Settings import __SCREENSHOT_FOLDER__ as screenshotFolder
# Import python modules
import os.path


# This handler will handle the requests send to '/photo'
# This handler will send screenshot back based on the filename
class PhotoHandler(BaseHandler):
    def processPostRequest(self, body):
        # Check json structure
        if 'operation' and 'params' in body and 'photoId' in body["params"]:
            # Generate file path depending on the passing photoId
            filePath = os.path.join(screenshotFolder, body["params"]["photoId"])
            # Is the path exists, send back image
            if os.path.exists(filePath):
                self.set_header("Content-Type", "image/jpeg")
                with open(filePath, 'rb') as f:
                    self.write(f.read())
            else:
                # If file not found, send 404 Not Found
                self.send_error(404)
        else:
            self.sendIncorrectRequestBodyResponse()
