# Import custom modules
from BaseHandler import BaseHandler
from Storage.Settings import __PERMITED_USER_AGENT_PREFIX__ as permitedUserAgentPrefix
import SharedInstances
# Import python modules
from cStringIO import StringIO
from Queue import Queue
# Import third party library
from PIL import Image
import tornado.web
import tornado.gen
import tornado


# This handler will handle requests send to path '/video'
class VideoStreamHandler(BaseHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        self.connectionClosed = False
        # if self.requestIsPermited() and self.authenticationCheck():
        self.frameQueue = Queue()
        SharedInstances.__VIDEO_PROCESSOR__.registNewReceiver(self.frameQueue)
        self.set_header('Content-type', 'multipart/x-mixed-replace; boundary=--jpgboundary')
        boundary = '--jpgboundary'
        while not self.connectionClosed:
            frame = self.frameQueue.get()
            jpgFormatImage = Image.fromarray(frame)
            jpgFile = StringIO()
            jpgFormatImage.save(jpgFile, "JPEG")
            self.write(boundary)
            self.write("Content-type: image/jpeg\r\n")
            self.write("Content-length: %s\r\n\r\n" % str(len(jpgFile.getvalue())))
            self.write(jpgFile.getvalue())
            yield tornado.gen.Task(self.flush)
        SharedInstances.__VIDEO_PROCESSOR__.unregistReceiver(self.frameQueue)

    # Different from the basehandler, we are using get method.
    # Therefore, there's no need to check content-type
    def requestIsPermitted(self):
        # Get interested headers
        userAgent = self.request.headers.get("User-Agent", None)
        cookie = self.request.headers.get("Cookie", None)
        # Return check results
        return (userAgent.startswith(permitedUserAgentPrefix)) and \
               (cookie is not None)

    # Handle the connection lost, stop the loop of the handler
    def on_connection_close(self):
        self.connectionClosed = True

    # Disable post access
    def post(self):
        self.send_error(403)
