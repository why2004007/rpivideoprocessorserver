# Import custom modules
from BaseHandler import BaseHandler
import SharedInstances
from Utilities import ResponseGenerator
from Storage import Settings
# Import third party module
import tornado.escape


# This handler will handle all request posted to '/sensor'
# This handler can return temperature and pressure readings
class SensorReadingHandler(BaseHandler):
    def __init__(self, application, request, **kwargs):
        self.logger = SharedInstances.__LOGGER_GENERATOR__.getLogger(__name__)
        BaseHandler.__init__(self, application, request, **kwargs)


    def processPostRequest(self, body):
        if 'operation' in body:
            if body["operation"] == 'get':
                temperature = SharedInstances.__SENSOR_MANAGER__.getCurrentTemperature()
                pressure = SharedInstances.__SENSOR_MANAGER__.getCurrentPressure()
                responseBody = {"temperature": temperature, "pressure": pressure}
                response = ResponseGenerator.generateResponse(Settings.__OK__, 'Readings retrieved.', [responseBody])
                self.write(tornado.escape.json_encode(response))
            else:
                self.sendIncorrectRequestBodyResponse()
        else:
            self.sendIncorrectRequestBodyResponse()
